import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  FlatList
} from "react-native";
import { values, color } from "./../../config";
import images from "./../../config/images";
import StarRate from "../../component/starRate";
import ReadingItem from "./ReadingItem";
import { inject, observer } from "mobx-react";
import cheerio from "cheerio-without-node-native";
import { Navigation } from "react-native-navigation";
@inject("Detail")
@observer
export default class ReadingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount = () => {};
  deleteFavorite = (item) => {
    let { Detail } = this.props;
    Detail.deleteFavorite(item);
  };
  renderReadingItem = ({ item, index }) => {
    return (
      <ReadingItem
        item={item}
        index={index}
        handleDetailBookScreen={this.handleDetailBookScreen}
      />
    );
  };

  handleDetailBookScreen = item => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "DetailBookScreen",
        id: "DetailBookScreen",
        passProps: {
          item: item
        },
        options: {
          topBar: {
            visible: false,
            height: 0
          },
          statusBar: {
            style: "light",
            visible: true
          }
        }
      }
    });
  };
  render() {
    let { Detail, item } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.viewTitle}>
          <Text style={styles.title}>Reading</Text>
          <TouchableOpacity
            style={{ padding: 10 }}
            onPress={() => {
              this.deleteFavorite();
            }}
          >
            <Image
              source={images.ic_delete}
              style={{ width: 20, height: 20, tintColor: "#fff" }}
            />
          </TouchableOpacity>
        </View>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderReadingItem}
          data={Detail.listFavorite}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewTitle: {
    height: 50,
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 20,
    justifyContent: "space-between",
    backgroundColor: "#F23434",
    width: "100%"
  },
  title: {
    color: "#fff",
    fontWeight: "500",
    fontSize: 18
  }
});
