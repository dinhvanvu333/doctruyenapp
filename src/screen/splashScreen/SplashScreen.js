import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Alert,
  NetInfo,
  AsyncStorage,
  PermissionsAndroid
} from "react-native";
import Permissions from "react-native-permissions";
import Provider from "../../utils/MobxRnnProvider";
import { goHome } from "../../../App";
console.disableYellowBox = true;
let isShowAlert = false;
let isGoLogin = false;
import { inject, observer } from "mobx-react";
import { values } from "../../config";
import images from "../../config/images";
@inject("User", "Home", "OnApp")
@observer
export default class SplashScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // permissionLocation: false
    };
    // this.getData = this.getData.bind(this);
    // this.requestLocationPermissionAccess = this.requestLocationPermissionAccess.bind(
    //   this
    // );
    // this.checkLocationPermissionAccess = this.checkLocationPermissionAccess.bind(
    //   this
    // );
    // this.requestLocationPermission = this.requestLocationPermission.bind(this);
    // this.checkNetWork = this.checkNetWork.bind(this);
  }
  // async requestLocationPermission() {
  //   try {
  //     const granted = await PermissionsAndroid.requestMultiple(
  //       [
  //         PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  //         PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
  //       ],
  //       {
  //         title: "Cool Photo App Camera Permission",
  //         message:
  //           "Cool Photo App needs access to your camera " +
  //           "so you can take awesome pictures."
  //       }
  //     );
  //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //       this.setState({
  //         permissionLocation: true
  //       });
  //     } else {
  //       this.setState({
  //         permissionLocation: false
  //       });
  //     }
  //   } catch (err) {
  //     console.warn(err);
  //   }
  //   try {
  //     const checkACCESS_FINE_LOCATION = await PermissionsAndroid.check(
  //       PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
  //     );
  //     const checkACCESS_COARSE_LOCATION = await PermissionsAndroid.check(
  //       PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
  //     );
  //     if (checkACCESS_FINE_LOCATION && checkACCESS_COARSE_LOCATION) {
  //       this.setState({
  //         permissionLocation: true
  //       });
  //     } else {
  //       this.setState({
  //         permissionLocation: false
  //       });
  //     }
  //   } catch (err) {
  //     console.warn(err);
  //   }
  // }

  // async getData() {
  //   let self = this;
  //   let { User, Home, OnApp } = this.props;
  //   await AsyncStorage.getItem("isTheFirst").then(isTheFirst => {
  //     //da login
  //     if (isTheFirst && isTheFirst == "true") {
  //       User.isTheFirst = true;
  //     } else {
  //       //chua vao app lan nao
  //       User.isTheFirst = false;
  //       AsyncStorage.setItem("isTheFirst", "true");
  //     }
  //   });
  //   await AsyncStorage.getItem("listCoordinate").then(listCoordinate => {
  //     //da login
  //     if (listCoordinate) {
  //       console.log(listCoordinate);
  //       // Home.setListCoords(listCoordinate)
  //     }
  //   });
  //   this.checkNetWork();
  // }

  // checkLocationPermissionAccess() {
  //   var self = this;
  //   Permissions.check("location", { type: "whenInUse" }).then(response => {
  //     console.log(
  //       " -------=========check quyen=======----------- " +
  //         JSON.stringify(response)
  //     );
  //     if (response != "authorized") {
  //       self.requestLocationPermissionAccess();
  //     } else {
  //       self.setState({ permissionLocation: true });
  //     }
  //   });
  // }

  // requestLocationPermissionAccess() {
  //   var self = this;
  //   var check = true;
  //   if (!isShowAlert) {
  //     Permissions.request("location", { type: "whenInUse" }).then(response => {
  //       if (response != "authorized") {
  //         isShowAlert = true;
  //         // Alert.alert('Open Settings', 'Please allow the app to use your location',
  //         Alert.alert(
  //           "Mở Cài đặt",
  //           "Vui lòng cho phép ứng dụng sử dụng vị trí của bạn",
  //           [
  //             {
  //               // text: 'Cancel',
  //               text: "Huỷ",
  //               onPress: () => {
  //                 isShowAlert = false;
  //                 self.requestLocationPermissionAccess();
  //               },
  //               style: "cancel"
  //             },
  //             {
  //               // text: 'Open Settings',
  //               text: "Mở Cài đặt",
  //               onPress: () => {
  //                 isShowAlert = false;
  //                 Permissions.openSettings();
  //                 check = true;
  //               }
  //             } //vao setting xin quyen
  //           ]
  //         );
  //       } else {
  //         self.setState({ permissionLocation: true });
  //       }
  //     });
  //   }
  // }

  // async checkNetWork() {
  //   let self = this;
  //   let checkNet = isConnected => {
  //     console.log("isConnectedddddddd", isConnected);
  //     if (isConnected) {
  //       //Check token
  //       this.props.OnApp.isConnect = true;
  //       setTimeout(() => {
  //         // self.onLetGo()
  //         goHome();
  //       }, 1000);
  //     } else {
  //       setTimeout(() => {
  //         this.props.OnApp.isConnect = false;
  //       }, 5000);
  //     }
  //   };
  //   NetInfo.isConnected.fetch().then(isConnected => checkNet(isConnected));
  //   NetInfo.isConnected.addEventListener("connectionChange", isConnected =>
  //     checkNet(isConnected)
  //   );
  // }

  // onLetGo() {
  //   let { User, Home } = this.props;
  //   if (!User.isTheFirst) {
  //     //nếu lần đầu vào app=> checck quyền ở trang login
  //     if (isGoLogin) {
  //     } else {
  //       goHome();
  //     }
  //   } else {
  //     if (User.isTheFirst && this.state.permissionLocation) {
  //       Home.getCurrentPosition();
  //       if (isGoLogin) {
  //       } else {
  //         goHome();
  //       }
  //     } else {
  //       if (values.platform == "ios") {
  //         this.checkLocationPermissionAccess();
  //       } else {
  //         this.requestLocationPermission();
  //       }
  //     }
  //   }
  // // }

  // clickLogin = () => {
  //   let { User } = this.props;
  //   User.setUserInfo({ fullname: "Tran Xuan Ai" });
  // };
  // clickHome = () => {
  //   let { User } = this.props;
  //   User.setUserInfo({ fullname: "Tran Xuan Ai" });
  //   goHome();
  // };
  // componentWillMount() {
  //   // let { Home } = this.props;
  //   // Home.getCurrentPosition()
  //   var self = this;
  //   if (values.platform === "ios") {
  //     self.checkLocationPermissionAccess();
  //   } else {
  //     self.requestLocationPermission();
  //   }
  // }

  componentDidMount() {
    // this.getData();

    setTimeout(() => {
      goHome();
    }, 1500);
    // this.loadGraphicCards();
  }
  // componentWillUnmount() {
  //   NetInfo.isConnected.removeEventListener("connectionChange", () => {});
  // }

  // loadGraphicCards = async page => {
  //   let items = [];
  //   const searchUrl = `http://www.nettruyen.com/hot`;
  //   const response = await fetch(searchUrl);
  //   const htmlString = await response.text();
  //   const $ = cheerio.load(htmlString);

  //   //   return $(".item").map((item, index) => ({
  //   //     title: item.find("img").attr("alt"),
  //   //     imageUrl: item.find("img").attr("src")
  //   //   }));
  //   // // };
  //   // console.log("abc", $(".item"));
  //   $(".item").each(function() {
  //     items.push({
  //       title: $(this)
  //         .find("img")
  //         .attr("alt"),
  //       url: $(this)
  //         .find("img")
  //         .attr("src")
  //     });
  //   });
  //   console.log(items);
  //   // return items;
  // };

  render() {
    let { User } = this.props;
    // let name = User.userInfo.fullname || "";
    return (
      <Provider>
        <Image
          source={{ uri: "https://kenh14cdn.com/2017/8-1496944586464.jpg" }}
          style={{ width: "100%", height: "100%",resizeMode:'cover' }}
        />
      </Provider>
    );
  }
}
