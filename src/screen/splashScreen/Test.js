import React, { Component } from 'react';
import {
  Dimensions,
  Image,
  ListView,
  PixelRatio,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import ParallaxScrollView from 'react-native-parallax-scroll-view';

class Test extends Component {
  constructor(props) {
    super(props);
    this.state =  {
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      }).cloneWithRows([
        'Simplicity Matters',
        'Hammock Driven Development',
        'Value of Values',
        'Are We There Yet?',
        'The Language of the System',
        'Design, Composition, and Performance',
        'Clojure core.async',
        'The Functional Database',
        'Deconstructing the Database',
        'Hammock Driven Development',
        'Value of Values'
      ])
    };
  }

  render() {
    const { onScroll = () => {} } = this.props;
    return (
      <ListView
        ref="ListView"
        style={styles.container}
        dataSource={ this.state.dataSource }
        renderRow={(rowData) => (
          <View key={rowData} style={ styles.row }>
            <Text style={ styles.rowText }>
              { rowData }
            </Text>
          </View>
         )}
        renderScrollComponent={props => (
          <ParallaxScrollView
            onScroll={onScroll}

            headerBackgroundColor="#333"
            stickyHeaderHeight={ STICKY_HEADER_HEIGHT }
            parallaxHeaderHeight={ PARALLAX_HEADER_HEIGHT }
            backgroundSpeed={10}

            renderBackground={() => (
              <View key="background">
                <Image source={{uri: 'https://i.ytimg.com/vi/P-NZei5ANaQ/maxresdefault.jpg',
                                width: window.width,
                                height: PARALLAX_HEADER_HEIGHT}}/>
                <View style={{position: 'absolute',
                              top: 0,
                              width: window.width,
                              backgroundColor: 'rgba(0,0,0,.4)',
                              height: PARALLAX_HEADER_HEIGHT}}/>
              </View>
            )}

            renderForeground={() => (
              <View key="parallax-header" style={ styles.parallaxHeader }>
                <Image style={ styles.avatar } source={{
                  uri: 'https://pbs.twimg.com/profile_images/2694242404/5b0619220a92d391534b0cd89bf5adc1_400x400.jpeg',
                  width: AVATAR_SIZE,
                  height: AVATAR_SIZE
                }}/>
                <Text style={ styles.sectionSpeakerText }>
                  Talks by Rich Hickey
                </Text>
                <Text style={ styles.sectionTitleText }>
                  CTO of Cognitec, Creator of Clojure
                </Text>
              </View>
            )}

            renderStickyHeader={() => (
              <View key="sticky-header" style={styles.stickySection}>
                <Text style={styles.stickySectionText}>Rich Hickey Talks</Text>
              </View>
            )}

            renderFixedHeader={() => (
              <View key="fixed-header" style={styles.fixedSection}>
                {/* <Text style={styles.fixedSectionText}
                      onPress={() => this.refs.ListView.scrollTo({ x: 0, y: 0 })}>
                  Scroll to top
                </Text> */}
              </View>
            )}/>
        )}
      />
    );
  }
}

const window = Dimensions.get('window');

const AVATAR_SIZE = 120;
const ROW_HEIGHT = 60;
const PARALLAX_HEADER_HEIGHT = 350;
const STICKY_HEADER_HEIGHT = 70;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black'
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: 300,
    justifyContent: 'flex-end'
  },
  stickySectionText: {
    color: 'white',
    fontSize: 20,
    margin: 10
  },
  fixedSection: {
    position: 'absolute',
    bottom: 10,
    right: 10
  },
  fixedSectionText: {
    color: '#999',
    fontSize: 20
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    paddingTop: 100
  },
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 18,
    paddingVertical: 5
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: 'white',
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center'
  },
  rowText: {
    fontSize: 20
  }
});

export default Test;
//Code
// import React, { Component } from "react";
// import {
//   Animated,
//   Image,
//   ImageBackground,
//   ScrollView,
//   SectionList,
//   StyleSheet,
//   Text,
//   TouchableOpacity,
//   View,
//   FlatList,
//   StatusBar
// } from "react-native";
// import { values, color } from "./../../../config";
// import images from "./../../../config/images";
// import StarRate from "../../../component/starRate";
// import { Navigation } from "react-native-navigation";
// import { inject, observer } from "mobx-react";
// import SectionItem from "./SectionItem";
// import styles from "./styles";
// import cheerio from "cheerio-without-node-native";
// import Loading from "./../../../component/Loading";
// @inject("Home")
// @observer
// export default class DetailBookScreen extends Component {
//   constructor(props) {
//     super(props);
//     this.scrollYAnimatedValue = new Animated.Value(0);
//     this.state = {
//       item: this.props.item,
//       dataSection: [],
//       isLoading: false
//       // showChapter: true
//     };
//   }
//   componentDidMount = () => {
//     StatusBar.setHidden(true);
//     this.loadNextPage();
//   };
//   loadGraphicCards = async () => {
//     let { item } = this.state;
//     let dataSection = [];
//     const searchUrl = item.linkComic;
//     const response = await fetch(searchUrl);
//     const htmlString = await response.text();
//     const $ = cheerio.load(htmlString);
//     console.log($(".list-chapter"));
//     $(".list-chapter .row a").each(function() {
//       dataSection.push({
//         Link: $(this)
//           .first()
//           .attr("href"),
//         chapter: $(this)
//           .first()
//           .text()
//       });
//     });
//     console.log(dataSection);
//     return dataSection;
//   };
//   loadNextPage = async () => {
//     let listDataSec = await this.loadGraphicCards();
//     this.setState({
//       dataSection: listDataSec
//     });
//   };
//   handleGoBack = () => {
//     Navigation.pop(this.props.componentId);
//   };
//   componentWillUnmount = () => {
//     let { item } = this.state;
//   };
//   renderSection = ({ item, index }) => {
//     return (
//       <SectionItem
//         item={item}
//         index={index}
//         handleGotoDetailChapter={this.handleGotoDetailChapter}
//       />
//     );
//   };
//   handleGotoDetailChapter = item => {
//     Navigation.push(this.props.componentId, {
//       component: {
//         name: "DetailChapter",
//         id: "DetailChapter",
//         passProps: {
//           item: item
//         },
//         options: {
//           topBar: {
//             leftButtons: [],
//             visible: false,
//             height: 0
//           },
//           statusBar: {
//             style: "light",
//             visible: true
//           }
//         }
//       }
//     });
//   };
//   render() {
//     const HEADER_MIN_HEIGHT = 0;
//     const HEADER_MAX_HEIGHT = values.deviceHeight / 1.8;
//     const ratingObj = {
//       ratings: 4
//     };
//     const headerHeight = this.scrollYAnimatedValue.interpolate({
//       inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
//       outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
//       extrapolate: "clamp"
//     });

//     const headerBackgroundColor = this.scrollYAnimatedValue.interpolate({
//       inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
//       outputRange: ["#00000020", "#00000020"],
//       extrapolate: "clamp"
//     });
//     const headerChange = this.scrollYAnimatedValue.interpolate({
//       inputRange: [0,values.deviceHeight / 1.8],
//       outputRange: [0, -300],
//       extrapolate: "clamp"
//     });
//     let { Home } = this.props;
//     let { item } = this.state;
//     return (
//       <View style={styles.main}>
//         <Animated.View
//           style={{
//             height: headerHeight,
//             backgroundColor: headerBackgroundColor,
//             // position: "absolute",
//             // top: Platform.OS == "ios" ? 20 : 0,
//             transform:[{translateY:headerChange}],
//             left: 0,
//             right: 0,
//             justifyContent: "center",
//             alignItems: "center"
//           }}
//         >
//           <ImageBackground
//             source={{
//               uri:
//                 item.url ||
//                 "http://www.huongtramvietnam.com/images/bigLoader.gif"
//             }}
//             style={styles.imgBgInterface}
//           />
//           <Animated.View style={styles.viewContent}>
//             <TouchableOpacity onPress={this.handleGoBack}>
//               <Image source={images.ic_back} style={styles.imgBack} />
//             </TouchableOpacity>
//             <Animated.View
//               style={{ justifyContent: "center", alignItems: "center" }}
//             >
//               <Image
//                 source={{
//                   uri:
//                     item.url ||
//                     "http://www.huongtramvietnam.com/images/bigLoader.gif"
//                 }}
//                 style={styles.imgItem}
//               />
//               <Animated.View
//                 style={{
//                   alignItems: "center",
//                   marginVertical: 20,
//                   width: (values.deviceWidth * 7) / 9
//                 }}
//               >
//                 <Text style={styles.titleFeature}>{item.title || ""}</Text>
//                 <View style={styles.content}>
//                   <Text style={{ color: "#848484", fontSize: 11 }}>
//                     {"andrew beria" || ""}
//                   </Text>
//                   <View style={styles.borderAmongView} />
//                   <Text style={{ color: "#848484", fontSize: 11 }}>12'45'</Text>
//                 </View>
//                 <StarRate ratingObj={ratingObj} />
//               </Animated.View>
//             </Animated.View>
//             <Animated.View>
//               <TouchableOpacity>
//                 <Image
//                   source={images.ic_enlarge}
//                   style={{ width: 15, height: 15, marginBottom: 20 }}
//                 />
//               </TouchableOpacity>
//               <TouchableOpacity>
//                 <Image
//                   source={images.ic_love}
//                   style={{ width: 15, height: 15 }}
//                 />
//               </TouchableOpacity>
//             </Animated.View>
//           </Animated.View>
//         </Animated.View>
//         <View style={styles.viewSectionList}>
//           <ScrollView
//             // contentContainerStyle={{ paddingTop: HEADER_MAX_HEIGHT }}
//             scrollEventThrottle={16}
//             onScroll={Animated.event([
//               {
//                 nativeEvent: { contentOffset: { y: this.scrollYAnimatedValue } }
//               }
//             ])}
//             showsVerticalScrollIndicator={false}
//           >
//             {/* <SectionList
//               renderItem={this.renderSection}
//               renderSectionHeader={({ section }) => (
//                 <View style={styles.viewContentTitleSectionList}>
//                   <Text style={styles.titleContentSectionList}>
//                     {section.title}{" "}
//                   </Text>
//                 </View>
//               )}
//               sections={Home.SectionListData}
//               keyExtractor={(item, index) => index.toString()}
//               showsVerticalScrollIndicator={false}
//             /> */}
//             <FlatList
//               keyExtractor={(item, index) => index.toString()}
//               renderItem={this.renderSection}
//               data={this.state.dataSection}
//               showsVerticalScrollIndicator={false}
//             />
//           </ScrollView>
//         </View>
//       </View>
//     );
//   }
// }
