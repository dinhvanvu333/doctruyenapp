import React, { Component } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity,Platform } from "react-native";
import images from "./../../config/images";
import { values, color } from "./../../config";
import ProfileInformation from "./ProfileInformation";
import ProfileFunction from "./ProfileFunction";
import ImagePicker from "react-native-image-picker";
import { Navigation } from "react-native-navigation";
const options = {
  title: "Select Avatar",
  customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};
export default class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Dinh Van Vu",
      avatarSource: "",
      photo: null
    };
  }
  handleAboutProfileScreen = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "AboutProfileScreen",
        id: "AboutProfileScreen",
        options: {
          topBar: {
            visible: false,
            height: 0
          },
          statusBar: {
            style: "light",
            visible: true
          },
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } })
          }
        }
      }
    });
  };
  handleFavoriteProfileScreen = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "FavoriteProfileScreen",
        id: "FavoriteProfileScreen",
        options: {
          topBar: {
            visible: false,
            height: 0
          },
          statusBar: {
            style: "light",
            visible: true
          },
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } })
          }
        }
      }
    });
  };

  handleSettingProfileScreen = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "SettingProfileScreen",
        id: "SettingProfileScreen",
        options: {
          topBar: {
            visible: false,
            height: 0
          },
          statusBar: {
            style: "light",
            visible: true
          },
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } })
          }
        }
      }
    });
  };

  handleChangeImage = async() => {
    // let { User } = this.props;
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        if (response.error === "Camera permissions not granted") {
          _alertForPhotosAndCameraPermission(
            "Bạn có thể cho ứng dụng truy cập máy ảnh",
            "Ứng dụng sử dụng máy ảnh để chụp ảnh"
          )
        } else if (response.error === "Photo library permissions not granted") {
          _alertForPhotosAndCameraPermission(
            "Bạn có thể cho ứng dụng truy cập thư viện ảnh",
            "Ứng dụng sử dụng thư viện ảnh để lấy ảnh"
          );
        }
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        // const source = response;
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({ photo: { uri: response.uri } });
        // await AsyncStorage.setItem("photo", JSON.stringify(photo));
      }
    }); 
  };
  render() {
    return (
      <View style={styles.main}>
        <View style={styles.viewAvatar}>
          <View>
            <View style={styles.viewImg}>
              <Image
                source={this.state.photo ? this.state.photo : images.ic_avatar}
                style={{ width: 80, height: 80 }}
              />
            </View>
            <TouchableOpacity
              onPress={this.handleChangeImage}
              style={{ position: "absolute", marginTop: 60 }}
            >
              <View>
                <Image
                  source={images.ic_camera}
                  style={{
                    width: 25,
                    height: 25,
                    marginLeft: 55,
                    tintColor: "#666666"
                  }}
                />
              </View>
            </TouchableOpacity>
          </View>
          <Text style={styles.name}>{this.state.name}</Text>
        </View>
        <View style={styles.viewInformation}>
          <ProfileInformation
            title="Type"
            content="Ordinary"
            style={{ backgroundColor: "#FF6B00" }}
          />
          <ProfileInformation
            title="Expiry Day"
            content="341"
            style={{ backgroundColor: "#0794EE", marginHorizontal: 20 }}
          />
          <ProfileInformation
            title="Flower"
            content="302,322"
            style={{ backgroundColor: "#EFC703" }}
          />
        </View>
        <ProfileFunction
          title="Favorites"
          image={images.ic_heart}
          style={{ marginTop: 40 }}
          onPress={this.handleFavoriteProfileScreen}
        />
        <ProfileFunction
          title="About us"
          image={images.ic_info}
          style={{ marginTop: 3 }}
          onPress={this.handleAboutProfileScreen}
        />
        <ProfileFunction
          title="Setting"
          image={images.ic_setting}
          style={{ marginTop: 3 }}
          onPress={this.handleSettingProfileScreen}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewInformation: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 20
  },
  name: {
    fontSize: 14,
    color: "#202122",
    marginTop: 10,
    fontWeight: "400"
  },
  viewImg: {
    width: 80,
    height: 80,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: "#CCCCCC",
    overflow: "hidden"
  },
  viewAvatar: {
    marginVertical: 40,
    alignItems: "center",
    justifyContent: "center",
    width: "100%"
  },
  main: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: "#F8F8F8"
  }
});
