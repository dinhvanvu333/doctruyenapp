import React, { Component } from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import { values, color } from "../../config";
export default class ProfileInformation extends Component {
  render() {
    let { title, content, style } = this.props;
    return (
        <View style={[styles.main, style]}>
          <Text style={{ fontSize: 12, color: "#FFFCF9" }}>{title}</Text>
          <Text style={{ fontSize: 16, color: "#FFFCF9", fontWeight: "500" }}>
            {content}
          </Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    alignItems: "center",
    justifyContent: "center",
    width: (values.deviceWidth - 80) / 3,
    height: values.deviceHeight / 7.5,
    borderRadius: 8
  }
});
