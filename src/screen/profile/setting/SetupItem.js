import React, { Component } from "react";
import { Text, View, Switch, StyleSheet, TouchableOpacity } from "react-native";

export default class SetupItem extends Component {
  render() {
    let { title, data } = this.props;
    return (
      <TouchableOpacity>
        <View style={styles.main}>
          <Text style={styles.title}>{title}</Text>
          <Text style={{ fontSize: 16, color: "#777777" }}>{data}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  title: { fontSize: 16, fontWeight: "500" },
  main: {
    alignItems: "center",
    borderBottomColor: "#e6e9ef",
    borderBottomWidth: 0.5,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 25,
    flex: 1,
    width: "100%",
    paddingHorizontal: 15
  }
});
