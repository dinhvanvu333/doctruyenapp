import React, { Component } from "react";
import { Navigation } from "react-native-navigation";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  Switch
} from "react-native";
import images from "./../../../config/images";
import { values, color } from "./../../../config";
import SetupSwitchItem from "./SetupSwitchItem";
import SetupItem from "./SetupItem";
export default class SettingProfileScreen extends Component {
  handleGoBack = () => {
    Navigation.pop(this.props.componentId);
  };
  render() {
    return (
      <View style={styles.main}>
        <View style={styles.viewGoBack}>
          <TouchableOpacity onPress={this.handleGoBack}>
            <Image source={images.ic_back} style={styles.imgBack} />
          </TouchableOpacity>
          <Text style={styles.title}>Settings</Text>
        </View>
        <View style={{ width: "100%" }}>
          <SetupSwitchItem title={"Push notification"} />
          <SetupSwitchItem title={"Download notification"} />
          <SetupSwitchItem title={"Mature Content Filter "} />
          <SetupItem title={"Clear cache"} data={"1.67M"} />
          <SetupItem title={"Update to latest version"} />
          <SetupItem title={"Data collection and instructions"} />
          <SetupItem title={"Export My data"} />
          <SetupItem title={"Delete data or cancel authorization"} />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  imgBack: {
    marginLeft: 20,
    height: 15,
    width: 15
  },
  main: { flex: 1, backgroundColor: "#FFFFFF" },
  title: {
    color: "#fff",
    fontWeight: "500",
    fontSize: 18,
    marginLeft: 10
  },
  viewGoBack: {
    alignItems: "center",
    backgroundColor: "#F23434",
    flexDirection: "row",
    height: 50,
    width: "100%"
  },
  titleSetup: { fontSize: 16, fontWeight: "500" },
  viewSetupItem: {
    justifyContent: "center",
    borderBottomColor: "#e6e9ef",
    borderBottomWidth: 0.5,
    paddingVertical: 25,
    flex: 1,
    width: "100%",
    paddingLeft: 15
  }
});
