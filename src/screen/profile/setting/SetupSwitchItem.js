import React, { Component } from "react";
import { Text, View, Switch, StyleSheet, TouchableOpacity } from "react-native";

export default class SetupSwitchItem extends Component {
  render() {
    let { title } = this.props;
    return (
      <View style={styles.main}>
        <Text style={styles.title}>{title}</Text>
        <Switch
          trackColor={"#CF001F"}
          tintColor={"#CF001F"}
          ios_backgroundColor={"#CF001F"}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: { fontSize: 16, fontWeight: "500" },
  main: {
    alignItems: "center",
    borderBottomColor: "#e6e9ef",
    borderBottomWidth: 0.5,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 25,
    flex: 1,
    width: "100%",
    paddingHorizontal: 15
  }
});
