import React, { Component } from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity } from "react-native";
import { values, color } from "../../config";
import images from "./../../config/images";
export default class ProfileFunction extends Component {
  render() {
    let { image, title, onPress,style } = this.props;
    return (
      <TouchableOpacity activeOpacity={0.7} onPress={onPress} style={{marginHorizontal:20}}>
        <View style={[styles.main,style]}>
          <View style={styles.viewLeft}>
            <Image source={image} style={styles.imgLeft} />
            <View style={{ flex: 1 }}>
              <Text style={styles.title}>{title || ""}</Text>
            </View>
          </View>
          <Image source={images.ic_next} style={styles.imgRight} />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  imgLeft: {
    height: 20,
    marginLeft: 20,
    width: 20
  },
  imgRight: {
    tintColor: "#9A9A9A",
    resizeMode: "contain",
    height: 15,
    marginRight: 20,
    width: 15
  },
  main: {
    borderRadius: 7,
    marginBottom: 7,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "space-between",
    height: values.deviceHeight/10,
    flexDirection: "row",
    width: "100%",
    paddingHorizontal: 10,
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: { width: 4, height: 4 },
    shadowOpacity: 0.4,
    shadowRadius: 7,
    elevation: 5
  },
  title: {
    paddingLeft: 10,
    width: "100%",
    color: "#303030",
    fontSize: 16
  },
  viewLeft: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row"
  }
});
