import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  FlatList
} from "react-native";
import { values, color } from "./../../../config";
import images from "./../../../config/images";
import StarRate from "../../../component/starRate";
import FavoriteItem from "./FavoriteItem";
import { inject, observer } from "mobx-react";
import cheerio from "cheerio-without-node-native";
import { Navigation } from "react-native-navigation";
@inject("Detail")
@observer
export default class FavoriteProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderReadingItem = ({ item, index }) => {
    return (
      <FavoriteItem
        item={item}
        index={index}
        handleDetailBookScreen={this.handleDetailBookScreen}
      />
    );
  };
  handleDetailBookScreen = item => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "DetailBookScreen",
        id: "DetailBookScreen",
        passProps: {
          item: item
        },
        options: {
          topBar: {
            visible: false,
            height: 0,
            leftButtons: {}
          },
          statusBar: {
            style: "light",
            visible: true
          }
        }
      }
    });
  };
  handleGoBack = () => {
    Navigation.pop(this.props.componentId);
  };
  render() {
    let { Detail } = this.props;
    return (
      <View style={styles.main}>
        <View style={styles.viewGoBack}>
          <TouchableOpacity onPress={this.handleGoBack}>
            <Image source={images.ic_back} style={styles.imgBack} />
          </TouchableOpacity>
          <Text style={styles.title}>Favorites</Text>
        </View>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderReadingItem}
          data={Detail.listFavorite}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imgBack: {
    marginLeft: 20,
    height: 15,
    width: 15
  },
  main: { flex: 1 },
  title: {
    color: "#fff",
    fontWeight: "500",
    fontSize: 18,
    marginLeft: 20
  },
  viewGoBack: {
    alignItems: "center",
    backgroundColor: "#F23434",
    flexDirection: "row",
    height: 50,
    width: "100%"
  }
});
