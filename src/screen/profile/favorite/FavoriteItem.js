import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  FlatList
} from "react-native";
import StarRate from "../../../component/starRate";
import images from "../../../config/images";
import { values, color } from "../../../config";
import Swipeout from "react-native-swipeout";

export default class FavoriteItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    var swiperSetting = {
      autoClose: true,
      onClose: (secId, rowId, direction) => {},
      onOpen: (secId, rowId, direction) => {},
      right: [
        {
          onPress: () => {},
          text: "Delete",
          type: "Delete",
          backgroundColor: "#F23434",
          color: "#fff"
        }
      ],
      rowId: this.props.index
    };
    const ratingObj = {
      ratings: 4
    };
    let { item, handleDetailBookScreen } = this.props;
    return (
      <Swipeout {...swiperSetting}>
        <TouchableOpacity
          onPress={() => {
            handleDetailBookScreen(item);
          }}
        >
          <View style={styles.main}>
            <View style={styles.body}>
              <Image
                source={{
                  uri:
                    item.url ||
                    "http://www.huongtramvietnam.com/images/bigLoader.gif"
                }}
                style={styles.imgBody}
              />
              <View style={styles.content}>
                <Text style={styles.titleFeature}>{item.title}</Text>
                <Text style={{ color: "#717171", fontSize: 10 }}>
                  {item.title}
                </Text>
                <View style={styles.title}>
                  <StarRate ratingObj={ratingObj} />
                  <Text style={styles.rate}>{item.rate}</Text>
                </View>
                <Text style={styles.kind}>{item.kind}</Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </Swipeout>
    );
  }
}
const styles = StyleSheet.create({
  body: {
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    elevation: 10,
    flexDirection: "row",
    height: values.deviceHeight / 6,
    marginBottom: 20,
    marginTop: 30,
    shadowColor: "#6CA3A8",
    shadowOffset: { width: 6, height: 6 },
    shadowOpacity: 0.4,
    shadowRadius: 2
    // width: "100%"
  },
  borderAmongView: {
    backgroundColor: "#000",
    height: 10,
    width: 1
  },
  content: {
    width: "65%",
    justifyContent: "space-between",
    marginLeft: 15,
    paddingVertical: 10
  },
  imgBody: {
    borderRadius: 8,
    height: values.deviceHeight / 5,
    marginTop: (-values.deviceHeight * 1) / 30,
    width: values.deviceWidth / 4
  },
  kind: {
    fontWeight: "500",
    fontSize: 10,
    color: "#717171"
  },
  main: {
    backgroundColor: "#F8F8F8",
    flex: 1,
    paddingHorizontal: 15
  },
  rate: {
    marginLeft: 5,
    color: "#A9AAAA",
    fontSize: 10
  },
  title: {
    alignItems: "center",
    flexDirection: "row"
  },
  titleFeature: {
    fontSize: 18,
    fontWeight: "500"
  }
});
