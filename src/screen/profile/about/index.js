import React, { Component } from "react";
import { Text, View, TouchableOpacity, Image, StyleSheet } from "react-native";
import images from "./../../../config/images";
import { values, color } from "./../../../config";
import { Navigation } from "react-native-navigation";
import LinkItems from "./LinkItem";
export default class AboutProfileScreen extends Component {
  handleGoBack = () => {
    Navigation.pop(this.props.componentId);
  };
  render() {
    return (
      <View style={styles.main}>
        <View style={styles.viewGoBack}>
          <TouchableOpacity onPress={this.handleGoBack}>
            <Image source={images.ic_back} style={styles.imgBack} />
          </TouchableOpacity>
          <Text style={styles.title}>Settings</Text>
        </View>
        <View
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <View style={styles.viewAvatar}>
            <Image source={images.ic_avatar} style={styles.avatar} />
          </View>
          <Text style={{ fontSize: 14, color: "#000", marginTop: 10 }}>
            Enjoy your comic time
          </Text>
          <Text style={{ fontSize: 14, color: "#000", marginVertical: 10 }}>
            Version 1.2.1.1
          </Text>
          <View style={styles.requestOne}>
            <Text style={{ fontSize: 14, color: "#419FD9" }}>Term of Use</Text>
          </View>
          <View style={styles.request}>
            <Text style={{ fontSize: 14, color: "#419FD9" }}>
              Privacy Policy
            </Text>
          </View>
          <LinkItems img={images.ic_fb} title={"@webComicsOfficial"} />
          <LinkItems img={images.ic_twitter} title={"@webComics_app"} />
          <LinkItems img={images.ic_instagram} title={"@webComics_Official"} />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  avatar: { width: 60, height: 60, borderRadius: 30 },
  viewAvatar: {
    alignItems: "center",
    backgroundColor: "#F23434",
    borderRadius: 15,
    height: values.deviceHeight / 7,
    justifyContent: "center",
    marginTop: 30,
    width: values.deviceWidth / 4.5
  },
  requestOne: {
    alignItems: "center",
    borderTopColor: "#e6e9ef",
    borderTopWidth: 0.5,
    justifyContent: "space-between",
    paddingVertical: 10,
    width: "100%"
  },
  request: {
    alignItems: "center",
    borderBottomColor: "#e6e9ef",
    borderTopColor: "#e6e9ef",
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    justifyContent: "space-between",
    paddingVertical: 10,
    width: "100%"
  },
  imgBack: {
    marginLeft: 20,
    height: 15,
    width: 15
  },
  main: { flex: 1 },
  title: {
    color: "#fff",
    fontWeight: "500",
    fontSize: 18,
    marginLeft: 20
  },
  viewGoBack: {
    alignItems: "center",
    backgroundColor: "#F23434",
    flexDirection: "row",
    height: 50,
    width: "100%"
  }
});
