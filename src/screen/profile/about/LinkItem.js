import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, StyleSheet } from "react-native";
import { values, color } from "./../../../config";
export default class LinkItems extends Component {
  render() {
    let { img, title } = this.props;
    return (
      <TouchableOpacity>
        <View style={styles.main}>
          <Image source={img} style={styles.img} />
          <Text style={styles.title}>{title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    borderBottomColor: "#00000060",
    borderBottomWidth: 0.5,
    color: "#00000080",
    fontSize: 16,
    fontWeight: "500",
  },
  img: { width: 30, height: 30, marginRight: 8 },
  main: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    paddingVertical: 10
  }
});
