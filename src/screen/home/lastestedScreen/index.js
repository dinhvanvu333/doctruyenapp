import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity
} from "react-native";
import BookItem from "./../../../component/bookItem";
import images from "./../../../config/images";
import { inject, observer } from "mobx-react";
import { Navigation } from "react-native-navigation";
import ListBook from "./../../../component/ListBook";
@inject("Home")
@observer
export default class LastestedScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: this.props.data
    };
  }

  handleGoBack = () => {
    Navigation.pop(this.props.componentId);
  };

  render() {
    let { Home } = this.props;
    return (
      <ListBook
        title="Lastested"
        data={this.state.data}
        onPress={this.handleGoBack}
      />
    );
  }
}
