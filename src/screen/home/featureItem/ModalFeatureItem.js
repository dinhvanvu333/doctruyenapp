import React, { Component } from "react";
import {
  Text,
  View,
  WebView,
  Image,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Platform
} from "react-native";
import images from "./../../../config/images";
import { inject, observer } from "mobx-react";
import { color, values } from "./../../../config";
import { Navigation } from "react-native-navigation";
@inject("Detail")
@observer
export default class ModalFeatureItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      item: this.props.item,
      imageFavorite: false
    };
  }
  handleDetailBookScreen = item => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "DetailBookScreen",
        id: "DetailBookScreen",
        passProps: {
          item: this.state.item
        },
        options: {
          topBar: {
            visible: false,
            height: 0
          },
          statusBar: {
            style: "light",
            visible: true
          },
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } })
          }
        }
      }
    });
  };
  handleSaveFavorites = item => {
    let { Detail } = this.props;
    Detail.handleSaveFavorites(item);
  };
  handleChangeColor = () => {
    this.setState({
      imageFavorite: true
    });
  };
  handleHideModal = () => {
    Navigation.dismissModal(this.props.componentId);
  };
  render() {
    let { item } = this.state;
    let { Detail } = this.props;
    return (
      <View style={styles.main}>
        <View style={styles.body}>
          <View style={styles.header}>
            <TouchableOpacity onPress={this.handleHideModal} style={{padding:10}}>
              <Image source={images.ic_close} style={styles.imgClose} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handleDetailBookScreen}>
              <Image source={images.ic_info} style={styles.imgAdd} />
            </TouchableOpacity>
          </View>
          <View style={styles.viewContent}>
            <View style={styles.viewTitleComic}>
              <Text style={styles.title}>{item.title}</Text>
              <Text style={styles.minute}>{item.LatestMinute}</Text>
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <WebView
              style={styles.webView}
              source={{
                uri:
                  item.linkComic ||
                  "http://www.huongtramvietnam.com/images/bigLoader.gif"
              }}
            />
          </View>
          <View style={styles.viewFavorite}>
            <TouchableOpacity
              onPress={() => {
                this.handleChangeColor();
                this.handleSaveFavorites(item);
              }}
            >
              <View
                style={{
                  shadowColor: "#000",
                  shadowOffset: { width: 5, height: 5 },
                  shadowOpacity: 0.8,
                  shadowRadius: 2,
                  elevation: 5,
                  width: 50,
                  height: 50
                }}
              >
                <Image
                  source={this.state.imageFavorite ? null : images.ic_heart}
                  style={styles.imgHeart}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imgHeart: { width: 50, height: 50, tintColor: "#FF6B00" },
  viewFavorite: {
    position: "absolute",
    height: "100%",
    width: "100%",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
    paddingBottom: 30,
    marginRight: 20
  },
  webView: { width: "100%", flex: 1 },
  minute: { color: "#FF6B00", fontSize: 11 },
  title: {
    fontSize: 16,
    fontWeight: "500",
    width: "75%"
  },
  viewContent: { width: "100%", marginBottom: 15 },
  imgAdd: { width: 25, height: 25, tintColor: "#FF6B00" },
  imgClose: { width: 20, height: 20 },
  viewTitleComic: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between"
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
    marginVertical: 20,
    borderBottomColor: "#DEDEDE",
    borderBottomWidth: 1,
    paddingBottom: 10
  },
  body: {
    backgroundColor: "#fff",
    marginTop: 50,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    flex: 1,
    width: "100%",
    paddingHorizontal: 20
  },
  main: { flex: 1, backgroundColor: "#00000030" }
});
