import React, { Component } from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity } from "react-native";
import { values, colors } from "../../config";
import images from "../../config/images";
import { Navigation } from "react-native-navigation";
export default class FeaturedItem extends Component {
  render() {
    let { item, gotoModalFeatureItem } = this.props;
    return (
      <View style={styles.main}>
        <TouchableOpacity
          onPress={() => {
            gotoModalFeatureItem(item);
          }}
        >
          <View>
            <Image
              source={{
                uri:
                  item.url ||
                  "http://www.huongtramvietnam.com/images/bigLoader.gif"
              }}
              style={styles.imgItem}
            />
            <View style={styles.viewRate}>
              <View style={styles.viewRateDetail}>
                <Image source={images.ic_star} style={styles.star} />
                <Text style={styles.starRate}>{"5" || ""}</Text>
              </View>
            </View>
          </View>
          <View style={{ height: 70 }}>
            <Text style={styles.titleFeature}>{item.title || ""}</Text>
          </View>
          {/* <View style={styles.kindFeatured}>
            <Text style={styles.kind}>{item.fees || ''}</Text>
          </View> */}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleFeature: { fontSize: 14, fontWeight: "400", marginTop: 10 },
  starRate: { fontSize: 10, color: "#FFFFFF" },
  star: { width: 8, height: 8, tintColor: "#FFFFFF" },
  kind: { color: "#FF6B00", fontSize: 11 },
  kindFeatured: {
    width: 40,
    height: 17,
    borderColor: "#FF6B00",
    borderWidth: 1,
    borderRadius: 10,
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 55,
    alignItems: "center"
  },
  viewRateDetail: {
    width: 20,
    height: 28,
    backgroundColor: "#FF6B00",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 5,
    marginRight: 5,
    borderRadius: 3
  },
  viewRate: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-end",
    position: "absolute"
  },
  imgItem: {
    width: values.deviceWidth / 3.5,
    height: values.deviceHeight / 5,
    borderRadius: 7
  },
  main: {
    width: values.deviceWidth / 3.5,
    marginRight: 15,
    flex: 1,
    marginBottom: 15
  }
});
