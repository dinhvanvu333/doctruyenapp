import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { values, color } from "../../config";
const styles = StyleSheet.create({
  imgAddFeature: { width: 10, height: 10 },
  titleFeature: {
    fontSize: 18,
    fontWeight: "500",
    marginBottom: 10,
    marginTop: 20
  },
  viewTitleFeature: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  viewFeatured: { width: "100%", paddingHorizontal: 20 },
  imgHeader: { height: 35, width: 35 },
  viewImgHeader: {
    borderRadius: 50,
    elevation: 5,
    height: 35,
    overflow: "hidden",
    resizeMode: "contain",
    shadowColor: "#000",
    shadowOffset: { width: 4, height: 4 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    width: 35
  },
  titleMain: { fontSize: 18, fontWeight: "500" },
  header: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 20,
    marginTop: 30
  },
  main: { flex: 1, width: "100%" }
});

export default styles;
