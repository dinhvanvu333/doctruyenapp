import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import { values, color, config } from "./../../../config";
import ImageZoom from "react-native-image-pan-zoom";
export default class FlatlistChapterItem extends Component {
  render() {
    let { item } = this.props;
    return (
      <View style={{ flex: 1, width: "100%" }}>
        <View style={{ width: "100%", alignItems: "center" }}>
          <ImageZoom
            cropWidth={values.deviceWidth}
            cropHeight={values.deviceHeight}
            imageWidth={values.deviceWidth - 20}
            imageHeight={500}
          >
            <Image
              style={{
                width: values.deviceWidth - 20,
                height: "100%",
                resizeMode: "contain"
              }}
              source={{
                uri:
                  item.pageChapter ||
                  "http://www.huongtramvietnam.com/images/bigLoader.gif"
              }}
            />
          </ImageZoom>
          {/* <Image
            source={{
              uri:
                item.pageChapter ||
                "http://www.huongtramvietnam.com/images/bigLoader.gif"
            }}
            style={{ width: values.deviceWidth-10, height: 500,resizeMode:'contain' }}
          /> */}
        </View>
      </View>
    );
  }
}
