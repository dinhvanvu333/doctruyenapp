import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  StyleSheet
} from "react-native";
import cheerio from "cheerio-without-node-native";
import FlatlistChapterItem from "./FlatlistChapterItem";
import { Navigation } from "react-native-navigation";
import { values, color, config } from "./../../../config";
import images from "./../../../config/images";
import { inject, observer } from "mobx-react";
@inject("Detail")
@observer
export default class DetailChapter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
      dataChapter: [],
      isLoading: true,
      imageFavorite: false
    };
  }
  componentDidMount = () => {
    this.loadNextPage();
  };
  loadGraphicCards = async () => {
    let { item } = this.state;
    let dataChapter = [];
    const searchUrl = item.Link;
    const response = await fetch(searchUrl);
    const htmlString = await response.text();
    const $ = cheerio.load(htmlString);
    $(".page-chapter").each(function() {
      dataChapter.push({
        pageChapter: $(this)
          .find("img")
          .attr("src")
      });
    });
    console.log(dataChapter);
    return dataChapter;
  };
  loadNextPage = async () => {
    let listDataChapter = await this.loadGraphicCards();
    this.setState({
      dataChapter: listDataChapter,
      isLoading: false
    });
  };
  handleChangeColor = () => {
    this.setState({
      imageFavorite: true
    });
  };
  // handleGoBack = () => {
  //   Navigation.pop(this.props.componentId);
  // };
  handleGoBack = () => {
    Navigation.dismissModal(this.props.componentId);
  };
  handleSaveFavorites = item => {
    let { Detail } = this.props;
    Detail.handleSaveFavorites(item);
  };
  renderChapterItem = ({ item, index }) => {
    return <FlatlistChapterItem item={item} index={index} />;
  };
  render() {
    let { item } = this.state;
    return (
      <View style={{ flex: 1,width:'100%',backgroundColor:'#fff' }}>
        <View style={styles.viewHeaderbar}>
          <TouchableOpacity onPress={this.handleGoBack} style={{ padding: 10 }}>
            <Image source={images.ic_back} style={styles.imgBack} />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 18,
              fontWeight: "600"
            }}
          >
            {item.chapter}
          </Text>
        </View>
        <View>
          {!this.state.isLoading ? (
            <FlatList
              keyExtractor={(item, index) => index.toString()}
              renderItem={this.renderChapterItem}
              data={this.state.dataChapter}
            />
          ) : (
            <View
              style={{
                flex:1,
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                backgroundColor:'#fafafafa',
                paddingTop:200
              }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  width: 70,
                  height: 70,
                  borderRadius: 15,
                  backgroundColor: "#00000030"
                }}
              >
                <ActivityIndicator size="large" color="#000" style={{tintColor:'#fff'}}/>
              </View>
            </View>
          )}
        </View>
        <View
          style={{
            position: "absolute",
            height: "100%",
            width: "100%",
            flexDirection: "row",
            alignItems: "flex-end",
            justifyContent: "flex-end",
            paddingBottom: 30,
            paddingRight: 20
          }}
        >
          <TouchableOpacity
            onPress={() => {
              this.handleChangeColor();
              this.handleSaveFavorites(item);
            }}
          >
            <View
              style={{
                shadowColor: "#000",
                shadowOffset: { width: 5, height: 5 },
                shadowOpacity: 0.8,
                shadowRadius: 2,
                elevation: 5,
                width: 50,
                height: 50
              }}
            >
              <Image
                source={this.state.imageFavorite ? null : images.ic_heart}
                style={{ width: 50, height: 50, tintColor: "#FF6B00" }}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imgBack: {
    width: 15,
    height: 15,
    tintColor: "#f9690e",
    marginHorizontal: 10
  },
  viewHeaderbar: {
    width: "100%",
    paddingHorizontal: 15,
    height: 45,
    flexDirection: "row",
    borderBottomColor: "#cacaca",
    borderBottomWidth: 1,
    backgroundColor: "#F23434",
    alignItems: "center"
  }
});
