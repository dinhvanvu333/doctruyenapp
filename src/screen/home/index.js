import React, { Component } from "react";
import {
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  StatusBar,
  ActivityIndicator,
  Platform
} from "react-native";
import { values, color } from "./../../config";
import images from "./../../config/images";
import Carousel from "react-native-snap-carousel";
import { inject, observer } from "mobx-react";
import CarouselITem from "./CarouselItem";
import FeaturedItem from "./FeaturedItem";
import BookItem from "./../../component/bookItem";
import { Navigation } from "react-native-navigation";
import styles from "./styles";
import cheerio from "cheerio-without-node-native";

@inject("Home")
@observer
export default class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true
    };
  }
  componentDidMount = () => {
    StatusBar.setHidden(true);
    this.loadNextPage();
  };
  loadGraphicCards = async () => {
    let data = [];
    const searchUrl = `http://www.nettruyen.com/tim-truyen/manhua`;
    const response = await fetch(searchUrl);
    const htmlString = await response.text();
    const $ = cheerio.load(htmlString);
    $(".item").each(function() {
      data.push({
        title: $(this)
          .find("img")
          .attr("alt"),
        url: $(this)
          .find("img")
          .attr("data-original"),
        linkComic: $(this)
          .find("a")
          .attr("href"),
        LatestMinute: $(this)
          .find("li")
          .find("i")
          .first()
          .text()
      });
    });
    return data;
  };
  loadNextPage = async () => {
    let listData = await this.loadGraphicCards();
    this.setState({
      data: listData
    });
  };

  handleGotoLatests = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "LastestedScreen",
        id: "LastestedScreen",
        passProps: {
          data: this.state.data
        },
        options: {
          topBar: {
            leftButtons: [],
            visible: false,
            height: 0
          },
          statusBar: {
            style: "light",
            visible: true
          },
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } })
          }
        }
      }
    });
  };
  handleDetailBookScreen = item => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "DetailBookScreen",
        id: "DetailBookScreen",
        passProps: {
          item: item
        },
        options: {
          topBar: {
            visible: false,
            height: 0
          },
          statusBar: {
            style: "light",
            visible: true
          },
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } })
          }
        }
      }
    });
  };
  gotoModalFeatureItem = item => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "ModalFeatureItem",
              passProps: {
                item: item
              },
              options: {
                modalPresentationStyle: "overCurrentContext",
                topBar: {
                  visible: false,
                  height: 0
                },
                layout: {
                  backgroundColor: "transparent"
                }
              }
            }
          }
        ]
      }
    });
  };
  handleGotoProfile = item => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "ProfileScreen",
        id: "ProfileScreen",
        options: {
          topBar: {
            visible: false,
            height: 0
          },
          statusBar: {
            style: "light",   
            visible: true
          },
          bottomTabs: { 
            visible: false,
            ...Platform.select({ android: { drawBehind: true } })
          }
        }
      }
    });
  };
  renderCarousel = ({ item, index }) => {
    return (
      <CarouselITem
        item={item}
        index={index}
        handleDetailBookScreen={this.handleDetailBookScreen}
      />
    );
  };
  renderFeaturedItem = ({ item, index }) => {
    return (
      <FeaturedItem
        item={item}
        index={index}
        gotoModalFeatureItem={this.gotoModalFeatureItem}
      />
    );
  };
  renderLatests = ({ item, index }) => {
    return (
      <BookItem
        item={item}
        index={index}
        handleDetailBookScreen={this.handleDetailBookScreen}
      />
    );
  };
  render() {
    let { Home } = this.props;
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.main}>
          <View style={{ width: "100%" }}>
            <View style={styles.header}>
              <Text style={styles.titleMain}>Home</Text>
              <TouchableOpacity onPress={this.handleGotoProfile}>
                <View style={styles.viewImgHeader}>
                  <Image source={images.ic_avatar} style={styles.imgHeader} />
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ width: "100%" }}>
              <Carousel
                ref={c => {
                  this._carousel = c;
                }}
                data={this.state.data}
                renderItem={this.renderCarousel}
                sliderWidth={values.deviceWidth}
                itemWidth={values.deviceWidth - 45}
                layout={"default"}
                autoplayInterval={1500}
                autoplay={true}
                enableMomentum={false}
                lockScrollWhileSnapping={true}
                loop={true}
              />
            </View>
          </View>
          <View style={styles.viewFeatured}>
            <View style={styles.viewTitleFeature}>
              <Text style={styles.titleFeature}>Featured</Text>
              <TouchableOpacity
                onPress={this.handleGotoLatests}
                style={{ height: 40, justifyContent: "center" }}
              >
                <Image source={images.ic_next} style={styles.imgAddFeature} />
              </TouchableOpacity>
            </View>
            <FlatList
              keyExtractor={(item, index) => index.toString()}
              renderItem={this.renderFeaturedItem}
              data={this.state.data}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            />
          </View>
          <View style={{ width: "100%", paddingHorizontal: 20 }}>
            <View style={styles.viewTitleFeature}>
              <Text style={styles.titleFeature}>Lastested</Text>
              <TouchableOpacity
                onPress={this.handleGotoLatests}
                style={{ height: 40, justifyContent: "center" }}
              >
                <Image source={images.ic_next} style={styles.imgAddFeature} />
              </TouchableOpacity>
            </View>
            <FlatList
              keyExtractor={(item, index) => index.toString()}
              renderItem={this.renderLatests}
              data={this.state.data}
              showsHorizontalScrollIndicator={false}
              ListFooterComponent={() =>
                this.state.isLoading ? (
                  <ActivityIndicator size="large" animating />
                ) : null
              }
              // ListHeaderComponent={() =>
              //   this.state.isLoading ? null : (
              //     <ActivityIndicator size="large" animating />
              //   )
              // }
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}
