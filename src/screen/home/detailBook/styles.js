import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { values, color } from "./../../../config";

const styles = StyleSheet.create({
  titleContentSectionList: { color: "#ACACAC", fontSize: 12, marginLeft: 20 },
  viewContentTitleSectionList: {
    height: 40,
    borderBottomColor: "#F2F2F2",
    borderBottomWidth: 1,
    justifyContent: "center"
  },
  viewSectionList: {
    // height: values.deviceHeight / 2,
    // paddingTop:values.deviceHeight / 1.8,
    flex: 1,
    width: "100%",
    shadowColor: "#000",
    shadowOffset: { width: 10, height: 10 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 8
  },
  borderAmongView: {
    width: 1,
    height: 10,
    backgroundColor: "#848484",
    marginHorizontal: 10
  },
  content: {
    flexDirection: "row",
    marginVertical: 5,
    alignItems: "center"
  },
  imgBack: { width: 15, height: 15, marginTop: 20 },
  viewContent: {
    // position: "absolute",
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%",
    paddingTop: 40,
    paddingHorizontal: 30
  },
  imgBgInterface: {
    width: "100%",
    height: "100%",
    resizeMode: "contain",
    opacity: 0.5
  },

  main: { flex: 1, width: "100%" },
  titleFeature: {
    fontSize: 18,
    fontWeight: "500",
    textAlign: "center",
    color: "#f22613"
  },
  starRate: { fontSize: 10, color: "#FFFFFF" },
  star: { width: 8, height: 8, tintColor: "#FFFFFF" },
  viewRateDetail: {
    width: 20,
    height: 28,
    backgroundColor: "#FF6B00",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 5,
    marginRight: 5,
    borderRadius: 3
  },
  viewRate: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-end",
    position: "absolute"
  },
  imgItem: {
    width: values.deviceWidth / 2.4,
    height: values.deviceHeight / 3.3,
    borderRadius: 7
  },
  viewShowImgBg: {
    alignItems: "center",
    // marginVertical: 20,
    width: (values.deviceWidth * 7) / 9
  },
  viewImgBg: {
    justifyContent: "center",
    alignItems: "center"
  },
  viewLoading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 50
  },
  titleComic: {
    color: "#ffffff",
    fontSize: 16,
    width: "75%"
  },
  imgBack: { width: 15, height: 15, tintColor: "#f9690e", marginTop: 15 },
  viewHeader: {
    height: 70,
    alignItems: "center",
    paddingLeft: 15,
    flexDirection: "row",
    backgroundColor: "#F23434"
  },
  imgLove: { width: 15, height: 15, tintColor: "#f9690e" },
  imgDownload: {
    width: 15,
    height: 15,
    tintColor: "#f9690e",
    marginVertical: 20
  }
});
export default styles;
