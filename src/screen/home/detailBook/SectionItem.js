import React, { Component } from "react";
import { Text, View, StyleSheet, TouchableOpacity, Image } from "react-native";
import images from "../../../config/images";
import { Navigation } from "react-native-navigation";
import DetailChapter from "./../detailChapter/index";
export default class SectionItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isShow: true,
      showChapter: true
    };
  }
  componentDidMount=()=>{
   
  }
  handleShowItem = item => {
    this.setState({
      isShow: !this.state.isShow
    });
  };

  render() {
    let { item } = this.props;
    return (
      <View>
        <TouchableOpacity onPress={this.handleShowItem}>
          {this.state.isShow ? (
            <View style={styles.main}>
              <Text style={styles.unit}>{item.chapter}</Text>
            </View>
          ) : (
            <TouchableOpacity
              onPress={() => {
                this.props.handleGotoDetailChapter(item);
                this.handleShowItem()
              }}
            >
              <View style={styles.main2}>
                <Text style={styles.unit}>{item.chapter}</Text>
                <Image
                  source={images.ic_songTab}
                  style={{ width: 15, height: 15 }}
                />
              </View>
            </TouchableOpacity>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main2: {
    alignItems: "center",
    backgroundColor: "#FFF0E5",
    flexDirection: "row",
    // height: 40,
    justifyContent: "space-between",
    paddingHorizontal: 20,
    width: "100%",
    borderBottomColor: "#00000060",
    borderBottomWidth: 0.5,
    paddingVertical: 10
  },
  unit: { color: "#9A9A9A", fontSize: 12, fontWeight: "300" },
  main: {
    // height: 40,
    justifyContent: "center",
    paddingLeft: 20,
    width: "100%",
    borderBottomColor: "#00000030",
    borderBottomWidth: 0.5,
    paddingVertical: 10
  }
});
