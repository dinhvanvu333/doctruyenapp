import React, { Component } from "react";
import {
  Animated,
  Image,
  ImageBackground,
  ScrollView,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  StatusBar,
  ActivityIndicator,
  AsyncStorage
} from "react-native";
import { values, color, config } from "./../../../config";
import images from "./../../../config/images";
import StarRate from "../../../component/starRate";
import { Navigation } from "react-native-navigation";
import { inject, observer } from "mobx-react";
import SectionItem from "./SectionItem";
import styles from "./styles";
import cheerio from "cheerio-without-node-native";
import { get, size, filter, find, findIndex, remove } from "lodash";
import ParallaxScrollView from "react-native-parallax-scroll-view";
@inject("Detail")
@observer
export default class DetailBookScreen extends Component {
  constructor(props) {
    super(props);
    this.scrollYAnimatedValue = new Animated.Value(0);
    this.state = {
      item: this.props.item,
      dataSection: [],
      isLoading: true,
      // imageFavorite: false,
      isFavorite: true
    };
  }
  componentDidMount = () => {
    StatusBar.setHidden(true);
    this.loadNextPage();
    this.checkFavorite();
  };
  checkFavorite = () => {
    const { Detail, item } = this.props;
    let listFavorite = Detail.listFavorite;
    let isCheck = listFavorite.some(val => val.title === item.title);
    if (isCheck) {
      this.setState({ isFavorite: true });
    } else {
      this.setState({ isFavorite: false });
    }
  };
  loadGraphicCards = async () => {
    let { item } = this.state;
    let dataSection = [];
    const searchUrl = item.linkComic;
    const response = await fetch(searchUrl);
    const htmlString = await response.text();
    const $ = cheerio.load(htmlString);
    $(".list-chapter .row a").each(function() {
      dataSection.push({
        Link: $(this)
          .first()
          .attr("href"),
        chapter: $(this)
          .first()
          .text()
      });
    });
    return dataSection;
  };
  loadNextPage = async () => {
    let listDataSec = await this.loadGraphicCards();
    this.setState({
      dataSection: listDataSec,
      isLoading: false
    });
  };
  handleGoBack = () => {
    Navigation.pop(this.props.componentId);
  };

  renderSection = ({ item, index }) => {
    return (
      <SectionItem
        item={item}
        index={index}
        handleGotoDetailChapter={this.handleGotoDetailChapter}
      />
    );
  };

  handleGotoDetailChapter = item => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "DetailChapter",
              passProps: {
                item: item
              },
              options: {
                topBar: {
                  visible: false,
                  height: 0
                },
                bottomTabs: {
                  visible: false
                }
              }
            }
          }
        ]
      }
    });
  };
  // handleChangeColor = () => {
  //   this.setState({
  //     isFavorite: !this.state.isFavorite
  //   });
  // };

  handleSaveFavorites = item => {
    let { Detail } = this.props;
    let { isFavorite } = this.state;
    // let arr = Detail.listFavorite;
    // let indexOfItem = arr.findIndex(arr.title === item.title);
    // console.log("===a===b", indexOfItem);
    if (isFavorite == true) {
      Detail.deleteFavoriteItem();
    } else {
      Detail.saveFavorite(item);
    }

    this.setState({
      isFavorite: !this.state.isFavorite
    });
    // if ((this.state.isFavorite = true)) {
    //   Detail.saveFavorite(item);
    // } else {
    //   Detail.deleteFavoriteItem();
    // }
  };
  render() {
    const ratingObj = {
      ratings: 4
    };
    let { item } = this.state;
    const PARALLAX_HEADER_HEIGHT = values.deviceHeight / 1.8;
    // const { onScroll = () => {} } = this.props;
    return (
      <View style={styles.main}>
        {/* <SectionList
              renderItem={this.renderSection}
              renderSectionHeader={({ section }) => (
                <View style={styles.viewContentTitleSectionList}>
                  <Text style={styles.titleContentSectionList}>
                    {section.title}{" "}
                  </Text>
                </View>
              )}
              sections={Home.SectionListData}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
            /> */}
        <ParallaxScrollView
          style={{ overflow: "hidden", flex: 1 }}
          // onScroll={onScroll}
          headerBackgroundColor="#333"
          stickyHeaderHeight={70}
          parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
          backgroundSpeed={15}
          showsVerticalScrollIndicator={false}
          renderBackground={() => (
            <ImageBackground
              source={{
                uri:
                  item.url ||
                  "http://www.huongtramvietnam.com/images/bigLoader.gif"
              }}
              style={styles.imgBgInterface}
            />
          )}
          renderForeground={() => (
            <View style={styles.viewContent}>
              <View>
                <TouchableOpacity
                  onPress={this.handleGoBack}
                  style={{ padding: 15 }}
                >
                  <Image source={images.ic_back} style={styles.imgBack} />
                </TouchableOpacity>
              </View>
              <View style={styles.viewImgBg}>
                <Image
                  source={{
                    uri:
                      item.url ||
                      "http://www.huongtramvietnam.com/images/bigLoader.gif"
                  }}
                  style={styles.imgItem}
                />
                <View style={styles.viewShowImgBg}>
                  <Text style={styles.titleFeature}>{item.title || ""}</Text>
                  <View style={styles.content}>
                    <Text style={{ color: "#fff", fontSize: 11 }}>
                      {"andrew beria" || ""}
                    </Text>
                    <View style={styles.borderAmongView} />
                    <Text style={{ color: "#fff", fontSize: 11 }}>12'45'</Text>
                  </View>
                  <StarRate ratingObj={ratingObj} />
                </View>
              </View>
              <View style={{}}>
                <TouchableOpacity style={{ padding: 10 }}>
                  <Image
                    source={images.ic_enlarge}
                    style={styles.imgDownload}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ padding: 10 }}
                  onPress={() => {
                    // this.handleChangeColor();
                    this.handleSaveFavorites(item);
                  }}
                >
                  <Image
                    source={
                      this.state.isFavorite ? images.ic_heart : images.ic_love
                    }
                    style={[styles.imgLove]}
                  />
                </TouchableOpacity>
              </View>
            </View>
          )}
          renderStickyHeader={() => (
            <View key="sticky-header" style={styles.viewHeader}>
              <View>
                <TouchableOpacity onPress={this.handleGoBack}>
                  <Image
                    source={images.ic_back}
                    style={{
                      width: 15,
                      height: 15,
                      marginRight: 8,
                      tintColor: "#f9690e"
                    }}
                  />
                </TouchableOpacity>
              </View>
              <Text style={styles.titleComic}>{item.title || ""}</Text>
            </View>
          )}
          renderFixedHeader={() => (
            <View
              key="fixed-header"
              style={{ position: "absolute", backgroundColor: "#F23434" }}
            />
          )}
        >
          {!this.state.isLoading ? (
            <View style={{ height: "100%" }}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                renderItem={this.renderSection}
                data={this.state.dataSection}
              />
            </View>
          ) : (
            <View style={styles.viewLoading}>
              <ActivityIndicator size="large" color="#000" />
            </View>
          )}
        </ParallaxScrollView>
      </View>
    );
  }
}
