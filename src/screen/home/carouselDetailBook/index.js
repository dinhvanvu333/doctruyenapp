import React, { Component } from "react";
import { Text, View } from "react-native";
import ListBook from "./../../../component/ListBook";
import { inject, observer } from "mobx-react";
@inject("Home")
@observer
export default class CarouselDetailBook extends Component {
  render() {
    let { Home } = this.props;
    return <ListBook title={Home.dataCarousel} data={Home.dataCarousel.data} />;
  }
}
