import React, { Component } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import { values } from "../../config";
export default class CarouselItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let { item,handleDetailBookScreen } = this.props;
    return (
      <TouchableOpacity
        onPress={() => {
          handleDetailBookScreen(item);
        }}
      >
        <View style={styles.main}>
          <Image
            source={{
              uri:
                item.url ||
                "http://www.huongtramvietnam.com/images/bigLoader.gif"
            }}
            style={styles.image}
          />
          <View style={styles.viewTitle}>
            <Text style={styles.title1}>{item.title || ""}</Text>
            {/* <Text style={styles.title2}>{item.title || ''}</Text> */}
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  viewTitle: {
    position: "absolute",
    backgroundColor: "#00000040",
    width: values.deviceWidth / 1.3,
    height: values.deviceHeight / 6.5,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 15,
    borderRadius: 10
  },
  title2: {
    fontSize: 30,
    color: "#F8F8F7",
    fontWeight: "500",
    marginTop: -10
  },
  title1: {
    fontSize: 16,
    color: "#F8F8F7",
    letterSpacing: 2,
    fontWeight: "500"
  },
  image: {
    width: values.deviceWidth - 50,
    height: values.deviceHeight / 5,
    borderRadius: 7
  },
  main: {
    width: "100%",
    justifyContent: "center",
    marginTop: 15,
    alignItems: "center"
  }
});
