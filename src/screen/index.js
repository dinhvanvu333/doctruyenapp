import SplashScreen from "./splashScreen/SplashScreen";
import ProfileScreen from "./profile/index";
import ReadingScreen from "./reading/index";
import MenuScreen from "./drawer/MenuScreen";
//Component
import StarRate from "../component/starRate";
//Home
import HomeScreen from "./home/";
import CarouselItem from "./home/CarouselItem";
import LastestedScreen from "./home/lastestedScreen/index";
import DetailBookScreen from "./home/detailBook/index";
import DetailChapter from "./home/detailChapter/index";
//Reading
import ReadingItem from "./reading/ReadingItem";
import Test from "./splashScreen/Test";
//Profile
import ProfileFunction from "./../screen/profile/ProfileFunction";
import AboutProfileScreen from "./profile/about/index";
import FavoriteProfileScreen from "./profile/favorite/index";
import SettingProfileScreen from "./profile/setting/index";
import ModalFeatureItem from './home/featureItem/ModalFeatureItem'
export {
  SplashScreen,
  ProfileScreen,
  ReadingScreen,
  //Component
  StarRate,
  //Home
  HomeScreen,
  LastestedScreen,
  MenuScreen,
  CarouselItem,
  DetailBookScreen,
  DetailChapter,
  //Reading
  ReadingItem,
  //Profile
  ProfileFunction,
  Test,
  AboutProfileScreen,
  FavoriteProfileScreen,
  SettingProfileScreen,
  ModalFeatureItem
};
