import React, { Component } from "react";
import { AsyncStorage, Alert } from "react-native";
import { observable, action } from "mobx";
// import { config, values, api } from "../config";
import { findIndex } from "lodash";
import moment from "moment";
class Detail {
  constructor() {
    this.init();
  }
  @observable token = "";

  @observable userInfo = { name: "Dinh Van Vu" };
  @observable isTheFirst = false;
  @observable isLogin = false;
  @observable rootNavigator = null;
  @observable _carousel = null;

  @action
  setRefCarousel(value) {
    this._carousel = value;
  }
  @action
  snapToItemCarouse(index) {
    this._carousel.snapToItem(index);
  }

  init = async () => {
    let data = await AsyncStorage.getItem("arr");
    if (!data) {
      return;
    }
    this.listFavorite = JSON.parse(data);
  };

  @action
  getUserInfo() {
    console.log(JSON.stringify(this.userInfo));
  }
  @observable listFavorite = [];
  @action
  deleteFavorite(item) {
    Alert.alert(
      "Xoá Favorite",
      "Bạn thực sự muốn xoá tất cả Favorite này chứ?",
      [
        {
          text: "Từ chối",
          onPress: () => console.log("Permission denied"),
          style: "cancel"
        },
        {
          text: "Xoá",
          onPress: () => {
            let arr = this.listFavorite.slice();
            this.listFavorite = arr;
            this.listFavorite.splice(item);
          }
        } //vao setting xin quyen
      ]
    );
  }

  @action
  saveFavorite = item => {
    Alert.alert("Thêm Favorite", "Bạn đồng ý thêm favorite này chứ?", [
      {
        text: "Không",
        onPress: () => console.log("Permission denied"),
        style: "cancel"
      },
      {
        text: "Có",
        onPress: async () => {
          let arr = this.listFavorite.slice();
          arr.push(item);
          this.listFavorite = arr;
          console.log("avcds", arr);
          await AsyncStorage.setItem("arr", JSON.stringify(arr));
        }
      }
    ]);
  };
  @action
  deleteFavoriteItem = () => {
    Alert.alert("Xóa Favorite", "Bạn chắc chắn Xóa favorite này chứ?", [
      {
        text: "Không",
        onPress: () => console.log("Permission denied"),
        style: "cancel"
      },
      {
        text: "Có",
        onPress: async item => {
          let arr = this.listFavorite.slice();
          this.listFavorite = arr;
          let indexOfItem = arr.indexOf(item);

          // let indexOfItem = findIndex(arr, function(o) {
          //   return o.title == item.title;
          // });
          console.log("===id==", indexOfItem);
          if (indexOfItem != undefined && indexOfItem + "" == "-1") {
            //da co item
            arr.splice(indexOfItem, 1);
            await AsyncStorage.setItem("arr", JSON.stringify(arr));
            console.log("===ab==assa", arr);
          } else {
            //chua co item
            // this.listUnit.push(item)
          }
          // let indexOfItem = arr.findIndex(arr.title === item.title);

          // this.listFavorite.splice(indexOfItem, 1);
          // for (i = 0; i < arr.length; i++)
          //   if (arr.length != 0 && arr[i].title === item.title) {
          //     let arr = this.listFavorite.slice();
          //     this.listFavorite = arr;
          //     this.listFavorite.splice(item);
          //   }
        }
      }
    ]);
  };
}

export default new Detail();
