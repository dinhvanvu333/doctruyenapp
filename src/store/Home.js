import React, { Component } from "react";
import { AsyncStorage } from "react-native";
import { observable, action } from "mobx";
import { config, values, api } from "../config";
import images from "../config/images";
import _ from "lodash";
import moment from "moment";
import { GetNoToken } from "../config/request";
import Axios from "axios";

class Home {
  @observable token = "";

  @observable userInfo = { name: "Dinh Van Vu" };
  @observable listCoordinate = [];
  @observable listLocation = [];
  @observable myPosition = values.POSITION_DEFAULT;
  @observable data = null;
  @observable dataCarousel = [
    {
      img: images.ic_carouselBook1,
      title1: "LISTENING",
      title2: "BOOKS"
    },
    {
      img: images.ic_carouselBook2,
      title1: "AUDIO",
      title2: "BOOKS"
    },
    {
      img: images.ic_carouselBook3,
      title1: "VINTAGE",
      title2: "BOOKS"
    }
  ];
  @observable dataFeatured = [
    {
      img: images.ic_truyen1,
      rate: 5.0,
      title: "Bách luyện thành tiên",
      fees: ""
    },
    {
      img: images.ic_truyen2,
      rate: 4.5,
      title:  "Phàm nhân tu tiên",
      fees: "drama"
    },
    {
      img: images.ic_truyen3,
      rate: 5.0,
      title: "Chư thiên ký",
      fees: "Free"
    },
    {
      img: images.ic_truyen4,
      rate: 3.5,
      title: "Pokemon",
      fees: "drama"
    },
    {
      img: images.ic_truyen5,
      rate: 4.0,
      title:  "Tây uyển mị ảnh",
      fees: "hot"
    }
  ];
  @observable dataLastested = [
    {
      img: images.ic_truyen1,
      describe:
        "At least 10 Bangladeshis were killed and seven others injured in a...",
      title: "Bách luyện thành tiên",
      hour: "12:44:21",
      fees: "free"
    },
    {
      img: images.ic_truyen2,
      describe:
        "Some one million Rohingyas face serious health risks due to acute...",
      title: "Phàm nhân tu tiên",
      hour: "12:44:21",
      fees: ""
    },
    {
      img: images.ic_truyen3,
      describe:
        "Some 463 violations of freedom of speech occurred in the country...",
      title: "Chư thiên ký",
      hour: "12:44:21",
      fees: "free"
    },
    {
      img: images.ic_truyen4,
      describe:
        "Bangladesh should change its approach in dealing with Myanmar, as...",
      title: "Pokemon",
      hour: "12:44:21",
      fees: ""
    },
    {
      img: images.ic_truyen5,
      describe:
        "At least 10 Bangladeshis were killed and seven others injured in a...",
      title: "Tây uyển mị ảnh",
      hour: "12:44:21",
      fees: "free"
    }
  ];
  @observable SectionListData = [
    {
      title: "Season 1",
      data: [
        { unit: "1. Coure 001" },
        { unit: "2. Coure 002" },
        { unit: "3. Coure 003" },
        { unit: "4. Coure 004" },
        { unit: "5. Coure 005" }
      ]
    },
    {
      title: "Season 2",
      data: [
        { unit: "1. Coure 001" },
        { unit: "2. Coure 002" },
        { unit: "3. Coure 003" },
        { unit: "4. Coure 004" },
        { unit: "5. Coure 005" }
      ]
    },
    {
      title: "Season 3",
      data: [
        { unit: "1. Coure 001" },
        { unit: "2. Coure 002" },
        { unit: "3. Coure 003" },
        { unit: "4. Coure 004" },
        { unit: "5. Coure 005" }
      ]
    }
  ];
  @observable dataReading = [
    {
      img: images.ic_carouselBook3,
      actor: "Sarah Pezez",
      title: "The Forgotten Hours",
      kind: "",
      rate: "4.5",
      rateStar: "3"
    },
    {
      img: images.ic_carouselBook2,
      actor: "Sarah Pezez",
      title: "End Game",
      kind: " Episode 3",
      rate: "3.5",
      rateStar: "3"
    },
    {
      img: images.ic_carouselBook1,
      actor: "Sarah Pezez",
      title: "The Book in a Forest",
      kind: " Episode 2",
      rate: "4.5",
      rateStar: "5"
    },
    {
      img: images.ic_carouselBook3,
      actor: "Sarah Pezez",
      title: "Whiskey in a TeaCup",
      kind: "",
      rate: "4.5",
      rateStar: "3"
    },
    {
      img: images.ic_carouselBook2,
      actor: "Sarah Pezez",
      title: "Start The Legend",
      kind: " Episode 1",
      rate: "4.5",
      rateStar: "4"
    }
  ];
  @action
  getUserInfo() {
    console.log(JSON.stringify(this.userInfo));
  }

  @action
  getDataWeather(city, callback = null) {
    // let url=`?q=${city}&appid=${apiKey}`

    let url =
      config.domain_api +
      `/weather?q=${city}&appid=${config.api_key}&units=${config.unit.c.units}`;
    console.log("url: " + url);
    GetNoToken(url, (data, status) => {
      if (status) {
        if (data) {
          console.log("weather-city: " + JSON.stringify(data));
        }
      } else {
      }
    });
  }

  @action
  getDataWeatherWithCoordinates(coordinates, callback = null) {
    // let url=`?q=${city}&appid=${apiKey}`
    if (coordinates && coordinates.latitude && coordinates.longitude) {
      let url =
        config.domain_api +
        `/weather?lat=${coordinates.latitude}&lon=${
          coordinates.longitude
        }&appid=${config.api_key}&units=${config.unit.c.units}`;
      console.log("url: " + url);
      GetNoToken(url, (data, status) => {
        if (status) {
          if (data) {
            console.log("weather-city coordinates: " + JSON.stringify(data));
          }
        } else {
          console.log(data);
        }
      });
    }
  }

  @action
  getDataWeatherWithCoordinates(coordinates, callback = null) {
    // let url=`?q=${city}&appid=${apiKey}`

    let url =
      config.domain_api +
      `/weather?lat=${coordinates.latitude}&lon=${
        coordinates.longitude
      }&appid=${config.api_key}&units=${config.unit.c.units}`;
    // console.log('url: ' + url)
    GetNoToken(url, (data, status) => {
      if (status) {
        if (data) {
          // console.log('weather-city coordinates: ' + JSON.stringify(data))
        }
      } else {
      }
    });
  }

  @action
  addLocation(data) {
    let listLocation = this.listLocation;
    // if(data.city.id==)
    let obj = _.find(this.listLocation, function(o) {
      return o.city.id == data.city.id;
    });
    if (!obj) {
      listLocation.push(data);
      this.listLocation = listLocation;
    }
  }

  @action
  setListCoords(data) {
    this.listCoordinate = data;
  }

  @action
  setListLocation(data) {
    this.listLocation = data;
  }

  @action
  getData7DaysWithCoordinates(coordinates, callback = null) {
    // let url=`?q=${city}&appid=${apiKey}`
    let listCoordinate = this.listCoordinate;
    //luu lai corrdinate

    let obj = _.find(this.listCoordinate, function(o) {
      return (
        o.latitude == coordinates.latitude &&
        o.longitude == coordinates.longitude
      );
    });
    if (!obj) {
      listCoordinate.push(coordinates);
      this.listCoordinate = listCoordinate;
      AsyncStorage.setItem(
        "listCoordinate",
        JSON.stringify(this.listCoordinate)
      );
    }

    const request_url = `https://api.openweathermap.org/data/2.5/forecast?lat=${
      coordinates.latitude
    }&lon=${coordinates.longitude}&appid=${config.api_key}&units=metric`;
    // console.log('link: ' + request_url)
    GetNoToken(request_url, (data, status) => {
      if (status) {
        if (data) {
          // console.log('Du lieu res.data: ' + JSON.stringify(data))
          this.addLocation(data);
          callback && callback(true);
          // this.checkIcon(res.data.list[0].weather[0].icon);
          // this.setState({
          //     temp: res.data.list[0].main.temp,
          //     temp_min: res.data.list[0].main.temp_min,
          //     temp_max: res.data.list[0].main.temp_max,
          //     city: res.data.city.name,
          //     firstTime: res.data.list[0].dt_txt.slice(0, 10),
          //     hourFirst: res.data.list[0].dt_txt.slice(11, 16),
          //     main: res.data.list[0].weather[0].description.toUpperCase(),
          //     arr: res.data.list.splice(1)
          // });
        } else {
          callback && callback(false);
        }
      } else {
        callback && callback(false);
      }
    });
  }

  @action
  getCurrentPosition() {
    navigator.geolocation.watchPosition(
      position => {
        console.log(position);
        if (position) {
          this.myPosition = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: values.LATITUDE_DELTA,
            longitudeDelta: values.LONGITUDE_DELTA
          };
          this.getDataWeatherWithCoordinates(this.myPosition, status => {});
          this.getData7DaysWithCoordinates(this.myPosition, status => {});
        } else {
          console.log("co loi gi do");
        }
      },
      error => {
        console.log("Không thể lấy được vị trí của bạn.");
      },
      {
        enableHighAccuracy: false, //false lấy theo wifi- true: GPS
        timeout: 20 * 1000,
        maximumAge: 5000
      }
    );
  }
}

export default new Home();
