import React, { Component } from "react";
import {
    AsyncStorage,
} from "react-native";
import { observable, action } from "mobx";
import {
    config,
    values,
    api,
} from "../config";
import _ from 'lodash'
import moment from 'moment'

class OnApp {
    @observable isConnect = false;
    //Trạng thái luồng ứng dụng; đang ở màn nào
    @observable myPosition = (values.POSITION_DEFAULT);
    @action setDataNoti(dataNoti) {
        this.dataNoti = dataNoti
        console.log("setDataNoti", dataNoti)
    }

   


}

export default new OnApp();
