import React, { Component } from "react";
import {
    AsyncStorage,
} from "react-native";
import { observable, action } from "mobx";
import {
    config,
    values,
    api,
} from "../config";
import _ from 'lodash'
import moment from 'moment'

class AddLocation {
    @observable token = '';
    @observable _carousel = null;
    @observable listAddress = [];

    @action
    setRefCarousel(value) {
        this._carousel = value
    }

    @action
    setListAddress(data) {
        this.listAddress = data;
    }
    @action
    clearListAddress() {
        this.listAddress = [];
    }

}

export default new AddLocation();
