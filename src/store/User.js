import React, { Component } from "react";
import {
  AsyncStorage,
} from "react-native";
import { observable, action } from "mobx";
import {
  config,
  values,
  api,
} from "../config";
import _ from 'lodash'
import moment from 'moment'

class User {
  @observable token = '';

  @observable userInfo = { 'name': 'Tran Xuan Ai' };
  @observable isTheFirst = false;
  @observable isLogin = false;
  @observable rootNavigator = null;

  @action
  getUserInfo() {
    console.log(JSON.stringify(this.userInfo))
  }


}

export default new User();
