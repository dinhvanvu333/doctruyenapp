import User from "./User";
import OnApp from "./OnApp";
import Home from "./Home";
import Detail from "./Detail";
import AddLocation from "./AddLocation";

const stores = {
  User,
  OnApp,
  Home,
  AddLocation,
  Detail
};

export default {
  ...stores
};
