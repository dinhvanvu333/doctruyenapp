import { Platform, Dimensions, PixelRatio } from "react-native";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;
const platformStyle = undefined;
const isIphoneX =
    platform === "ios" && deviceHeight === 812 && deviceWidth === 375;
const isAPILest21 =
    platform === 'android' && Platform.Version < 21;
const aspectRatio = deviceHeight / deviceWidth;
const deviceIsIphone = aspectRatio > 1.6 ? true : false;

//for map

// const ASPECT_RATIO = width / height;
let LATITUDE = 21.028667;
let LONGITUDE = 105.852148;
let LATITUDE_DELTA = 0.05;
let LONGITUDE_DELTA = LATITUDE_DELTA * aspectRatio;
let POSITION_DEFAULT = {
    latitude: LATITUDE,
    longitude: LONGITUDE,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA
};
let MARKER_ME_DEFAULT = {
    latitude: LATITUDE,
    longitude: LONGITUDE
}
const values = {
    POSITION_DEFAULT, MARKER_ME_DEFAULT,
    platform, isIphoneX, deviceIsIphone, isAPILest21,
    deviceHeight, deviceWidth, aspectRatio,
    status_bar_height_android: 24,
    contentToolbarHeight: platform === "ios" ? 44 : 44,
    toolbar: 45,
    marginTopScreen: platform === "ios" ? (isIphoneX ? 35 : 20) : 0,
    toolbarHeight: platform === "ios" ? (isIphoneX ? 74 : 64) : (isAPILest21 ? 44 : 60),//56
    nav: {
        fontSize: 21,

    }

};
export default values