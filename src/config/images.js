let images = {};
images.ic_home = require("./../assets/icon/home.png");
images.ic_book = require("./../assets/icon/book.png");
images.ic_user = require("./../assets/icon/profile.png");
images.ic_avatar = require("./../assets/images/avatar.jpg");
//Home
images.ic_carouselBook1 = require("./../assets/images/bookOrange.jpg");
images.ic_carouselBook2 = require("./../assets/images/bookbaby.jpg");
images.ic_carouselBook3 = require("./../assets/images/bookVintage.jpg");
images.ic_star1 = require("./../assets/icon/Star.png");
images.ic_star2 = require("./../assets/icon/star-line.png");
images.ic_next = require("./../assets/icon/inr-chevron-right.png");
images.ic_back = require("./../assets/icon/left-arrow.png");
images.ic_lastested1 = require("./../assets/images/Lastested1.jpg");
images.ic_lastested2 = require("./../assets/images/Lastested2.jpg");
images.ic_lastested3 = require("./../assets/images/Lastested3.jpg");
images.ic_close = require("./../assets/images/ic_close.png");
images.ic_add = require("./../assets/images/ic_add.png");
images.ic_info = require("./../assets/images/info.png");

//Detail
images.ic_enlarge = require("./../assets/icon/ic_arrow_upward.png");
images.ic_love = require("./../assets/icon/ic_favorite.png");
images.ic_songTab = require("./../assets/icon/song-tag.png");
images.ic_truyen1 = require("./../assets/images/truyen1.jpg");
images.ic_truyen2 = require("./../assets/images/truyen2.jpg");
images.ic_truyen3 = require("./../assets/images/truyen3.jpg");
images.ic_truyen4 = require("./../assets/images/truyen4.jpg");
images.ic_truyen5 = require("./../assets/images/truyen5.jpg");
images.ic_delete = require("./../assets/images/delete.png");
images.ic_heart = require("./../assets/images/ic_heart.png");
//Profile
images.ic_heart = require("./../assets/icon/heart-circle.png");
images.ic_info = require("./../assets/icon/info-circle.png");
images.ic_setting = require("./../assets/icon/setting-circle.png");
images.ic_camera = require("./../assets/icon/camera.png");
images.ic_fb = require("./../assets/icon/fb.png");
images.ic_instagram = require("./../assets/icon/instagram.png");
images.ic_twitter = require("./../assets/icon/twitter.png");

export default images;
