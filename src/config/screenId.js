export default {
    HOME: {
        id: 'HOME',
        screenType: {
            start: 'start', chooseCar: 'chooseCar',
            findCar: 'findCar', carIsComming: 'carIsComming', complete: 'complete'
        }
    }
};