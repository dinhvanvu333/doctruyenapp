export default {
    navbar: '#151519',
    backgroundColor: '#232429',
    LOGIN: {
        buttonColor: '#424B59',
        placehoderColor: '#D1D4DB',
        borderColor: '#8B939B',
        textFacebook: '#646B73',
        textLogin: '#AAB2BF'
    },
    HOME: {
        bgColorHome: '#303643',
        colorWeather: '#C7CACF',
        colorDescription: '#878A91',
        bgcolorscreen2: '#303644',
        colortext: '#C8C9CB'
    },

    ADD_LOCATION: {
        color_main: '#232429',
        color_nav: '#151519',
        color_text_input: '#232429',
    }


}