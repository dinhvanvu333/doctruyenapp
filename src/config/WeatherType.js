export default {
    TYPE_OF_WEATHER: {
        Rainy: {
            id: 0,
            title: 'Rainy',
            backgroundColor: '',
            icon: '',
        },

        Stormy: {
            id: 1,
            title: 'Stormy',
            backgroundColor: '',
            icon: '',
        },
    Sunny: {
        id: 2,
        title: 'Sunny',
        backgroundColor: '',
        icon: '',
    },
    Cloudy: {
        id: 3,
        title: 'Cloudy',
        backgroundColor: '',
        icon: '',
    },
    Hot: {
        id: 4,
        title: 'Hot',
        backgroundColor: '',
        icon: '',
    },
    Cold: {
        id: 5,
        title: 'Cold',
        backgroundColor: '',
        icon: '',
    },
    Dry: {
        id: 6,
        title: 'Dry',
        backgroundColor: '',
        icon: '',
    },
    Wet: {
        id: 7,
        title: 'Wet',
        backgroundColor: '',
        icon: '',
    },
    Windy: {
        id: 8,
        title: 'Windy',
        backgroundColor: '',
        icon: '',
    },
    Hurricanes: {
        id: 9,
        title: 'Hurricanes',
        backgroundColor: '',
        icon: '',
    },
    typhoons: {
        id: 10,
        title: 'typhoons',
        backgroundColor: '',
        icon: '',
    },
    SandStorms: {
            id: 11,
            title: 'Sand-storms',
            backgroundColor: '',
            icon: '',
        }
    SnowStorms: {
        id: 12,
        title: 'Snow-Storms',
        backgroundColor: '',
        icon: '',
    },
Tornados: {
    id: 13,
    title: 'Tornados',
    backgroundColor: '',
    icon: '',
},
Humid: {
    id: 14,
    title: 'Humid',
    backgroundColor: '',
    icon: '',
},
Foggy: {
    id: 15,
    title: 'Foggy',
    backgroundColor: '',
    icon: '',
},
Snow: {
    id: 16,
    title: 'Snow',
    backgroundColor: '',
    icon: '',
},
Thundersnow: {
    id: 17,
    title: 'Thunder-snow',
    backgroundColor: '',
    icon: '',
},
Hail: {
    id: 18,
    title: 'Hail',
    backgroundColor: '',
    icon: '',
},
Sleet: {
    id: 19,
    title: 'Sleet',
    backgroundColor: '',
    icon: '',
},
drought: {
    id: 20,
    title: 'drought',
    backgroundColor: '',
    icon: '',
},
wildfire: {
    id: 21,
    title: 'wild-fire',
    backgroundColor: '',
    icon: '',
},
blizzard: {
    id: 22,
    title: 'blizzard',
    backgroundColor: '',
    icon: '',
},
avalanche: {
    id: 23,
    title: 'avalanche',
    backgroundColor: '',
    icon: '',
},
Mist: {
    id: 24,
    title: 'Mist',
    backgroundColor: '',
    icon: '',
},
    }
}