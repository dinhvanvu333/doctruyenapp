import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet } from "react-native";
import images from "./../config/images";
import { values, color } from "./../config";
export default class BookItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let { item,handleDetailBookScreen } = this.props ;
    return (
      <TouchableOpacity onPress={() => {
        handleDetailBookScreen(item);
      }}>
        <View style={styles.main}>
          <View style={styles.viewImg}>
            <Image  
              source={{
                uri:
                  item.url ||
                  "http://www.huongtramvietnam.com/images/bigLoader.gif"
              }}
              style={styles.image}
            />
          </View>
          <View style={{ marginLeft: 20, flex: 75,justifyContent:'space-between' }}>
            <View>
              <Text style={{ fontSize: 16, fontWeight: "500" }}>
                {item.title || ""}
              </Text>
              <Text style={{ color: "#6B6B6B", fontSize: 14, marginBottom: 5 }}>
                {/* {item.describe || ''} */}
                {item.title || ""}
              </Text>
            </View>
            <View style={styles.footerContent}>
              <Text style={{ color: "#9A9A9A", fontSize: 11 }}>
                {item.hour || ""}
              </Text>
              {/* <View style={styles.kind}>
                <Text style={{ color: "#FF6B00", fontSize: 12 }}>
                  {item.fees || ""}
                </Text>
              </View> */}
               <Text style={{ color: "#FF6B00", fontSize: 11 }}>
                {item.LatestMinute || ""}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  kind: {
    width: 40,
    height: 18,
    borderColor: "#FF6B00",
    borderWidth: 1,
    borderRadius: 8,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  footerContent: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  image: {
    // resizeMode:'contain',
    width: values.deviceWidth / 5,
    height: values.deviceHeight / 7
  },
  viewImg: {
    // flex:25,
    shadowColor: "#000",
    shadowOffset: { width: 8, height: 8 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 7,
    width: values.deviceWidth / 5,
    height: values.deviceHeight / 7,
    overflow: "hidden"
  },
  main: {
    flex: 1,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 20
  }
});
