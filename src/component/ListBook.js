import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity
} from "react-native";
import BookItem from "./bookItem";
import images from "./../config/images";
import { inject, observer } from "mobx-react";
import { Navigation } from "react-native-navigation";
@inject("Home")
@observer
export default class ListBook extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  renderLatests = ({ item, index }) => {
    return (
      <BookItem
        item={item}
        index={index}
        handleDetailBookScreen={this.handleDetailBookScreen}
      />
    );
  };
  handleDetailBookScreen = item => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "DetailBookScreen",
        id: "DetailBookScreen",
        passProps: {
          item: item
        },
        options: {
          topBar: {
            visible: false,
            height: 0
          },
          statusBar: {
            style: "light",
            visible: true
          }
        }
      }
    });
  };
  render() {
    let { data, title, onPress } = this.props;
    return (
      <View style={{ width: "100%", paddingHorizontal: 20, flex: 1 }}>
        <TouchableOpacity style={styles.buttonBack} onPress={onPress}>
          <Image source={images.ic_back} style={{ width: 15, height: 15 }} />
        </TouchableOpacity>
        <View style={styles.viewTitleFeature}>
          <Text style={styles.titleFeature}>{title}</Text>
        </View>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderLatests}
          data={data}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  buttonBack: {
    width: 40,
    height: 40,
    justifyContent: "center",
    marginVertical: 10
  },
  titleFeature: {
    fontSize: 18,
    fontWeight: "500",
    marginBottom: 20
  },
  viewTitleFeature: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }
});
