import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import images from "../config/images";
export default class StarRate extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // Recieve the ratings object from the props
    let ratingObj = this.props.ratingObj;

    // This array will contain our star tags. We will include this
    // array between the view tag.
    let stars = [];
    // Loop 5 times
    for (var i = 1; i <= 5; i++) {
      // Set the path to filled stars
      let path = images.ic_star1;
      // If ratings is lower, set the path to unfilled stars
      if (i > ratingObj.ratings) {
        path = images.ic_star2;
      }
      // Push the Image tag in the stars array
      stars.push(
        <Image
          style={{ width: 12, height: 12, marginRight: 3, resizeMode: "cover" }}
          source={path}
        />
      );
    }
    return (
      <View
        style={{
          height: 20,
          flexDirection: "row",
          alignItems: "center"
        }}
      >
        {stars}
      </View>
    );
  }
}
