import React, { Component } from "react";
import _ from 'lodash'
import { ImageBackground, View, StatusBar, Image, Alert, Platform } from "react-native";
// import Permissions from 'react-native-permissions'
import SimpleToast from 'react-native-simple-toast'
import { values, api_key_google, config, status_api_google, } from "../config";
import { GetNoToken, GetWithToken } from '../config/request'
export const clog = (message, data) => {
    console.log(`${message}: ` + JSON.stringify(data));
};

export function converTempUnit(temp, type) {
    // °F  =  ( °C × 1.8 ) +  32
    // °C  =  ( °F ─  32 )  ⁄  1.8
    let result = null;
    if (type == config.unit.c.units) {//c=>F
        result = (parseFloat(temp + '') * 1.8 + 32).toFixed(2)
    } else {//F=>C
        result = ((parseFloat(temp + '') - 32) / 1.8).toFixed(2)
    }
    return result;
}

export function getListSuggestAddress(api_key_map, text) {
    let type = 'json';//loai output tra ve
    {
        console.log("https://maps.googleapis.com/maps/api/place/autocomplete/"
            + type
            + "?input=" + text
            + "&types=geocode"
            + "&language=vi"
            + "&components=country:vn|"
            + ""
            + "&key=" + api_key_map)
    }
    return "https://maps.googleapis.com/maps/api/place/autocomplete/"
        + type
        + "?input=" + text
        + "&types=geocode"
        + "&language=vi"
        + "&components=country:vn|"
        + ""
        + "&key=" + api_key_map;
}

export function urlGetLocationFromPlaceId(key_api_map, placeid) {
    return "https://maps.googleapis.com/maps/api/place/details/json?key=" + key_api_map + "&placeid=" + placeid + "&language=vi"
}

export function getLocationFromPlaceId(placeId, api_key_google, callback) {
    let error = 'Có lỗi xảy ra!'
    if (placeId) {
        GetNoToken(urlGetLocationFromPlaceId(api_key_google, placeId), (data, status) => {
            // console.log('lấy location từ placeID: ' + JSON.stringify(data))
            if (status) {
                if (data && data.status && data.status == 'OK') {
                    callback(data, true)
                } else {
                    callback(error, false)
                }
            } else {
                callback(error, false)
            }
        })
    }
};

export function getSuggestAddress(text, api_key_google, callback) {
    GetNoToken(getListSuggestAddress(api_key_google, text), (data, status) => {
        // console.log('------- ======== ========--goi y: ======== ======== ======== getSuggestAddress: ' + JSON.stringify(data))
        if (status) {
            if (data.status == status_api_google.OK) {
                callback(data.predictions, true)
            } else if (data.status == status_api_google.ZERO_RESULTS) {
                callback({ data: [], status: status_api_google.ZERO_RESULTS }, false)
            } else if (data.status == status_api_google.OVER_QUERY_LIMIT) {
                callback({ data: [], status: status_api_google.OVER_QUERY_LIMIT }, false)
            } else {
                callback({ data: [], status: data.status }, false)
            }
        } else {
            SimpleToast.show('Lỗi kết nối!')
            callback({ data: [], status: 'Lỗi kết nối' }, false)
        }
    })
};
