import { Navigation } from "react-native-navigation";
import { AppRegistry } from "react-native";
import { registerScreens } from "./src/screen/RegisterScreens";
import store from "./src/store";
import Provider from "./src/utils/MobxRnnProvider";
import images from "./src/config/images";
registerScreens(store, Provider);

export const goHome = () =>
  Navigation.setRoot({
    root: {
      bottomTabs: {
        children: [
          {
            stack: {
              children: [
                {
                  component: {
                    name: "HomeScreen",
                    options: {
                      topBar: {
                        leftButtons: [],
                        rightButtons: [],
                        visible: false,
                        height: 0
                      },
                      statusBar: {
                        style: "light",
                        visible: true
                      },
                      bottomTab: {
                        icon: images.ic_home,
                        color: "#FF6B00",
                        selectedIconColor: "#FF6B00",
                        text: "Home",
                        selectedTextColor: "#FF6B00",
                        fontSize: 7
                      },
                      layout: {
                        orientation: "portrait"
                      }
                    }
                  }
                }
              ]
            }
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: "ReadingScreen",
                    options: {
                      topBar: {
                        rightButtons: [],
                        visible: false,
                        height: 0
                      },
                      statusBar: {
                        style: "light",
                        visible: true
                      },
                      bottomTab: {
                        icon: images.ic_book,
                        selectedIconColor: "#FF6B00",
                        color: "#FF6B00",
                        text: "Reading",
                        selectedTextColor: "#FF6B00",
                        fontSize: 7
                      },
                      layout: {
                        orientation: "portrait"
                      }
                    }
                  }
                }
              ]
            }
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: "ProfileScreen",
                    options: {
                      topBar: {
                        rightButtons: [],
                        visible: false,
                        height: 0
                      },
                      statusBar: {
                        style: "dark",
                        visible: true
                      },
                      bottomTab: {
                        icon: images.ic_user,
                        color: "#FF6B00",
                        text: "Profile",
                        selectedTextColor: "#FF6B00",
                        selectedIconColor: "#FF6B00",
                        fontSize: 7,
                        
                      },
                      layout: {
                        orientation: "portrait"
                      }
                    }
                  }
                }
              ]
            }
          }
        ],
        options: {
          bottomTabs: {
            currentTabIndex: 0,
            titleDisplayMode: "alwaysShow",
            layout: {
              orientation: "portrait"
            },
            statusBar: {
              style: "light",
              visible: true
            }
          }
        }
      }
    }
  });

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      component: {
        name: "SplashScreen",
        // name:"Test"
      }
    }
  });
});
